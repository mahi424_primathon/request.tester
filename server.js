const express = require("express");
const app = express();
const cors = require("cors");


// function corsMiddleware(req, res, next) {
//   // Website you wish to allow to connect
//   res.setHeader("Access-Control-Allow-Origin", "*");

//   // Request methods you wish to allow
//   res.setHeader(
//     "Access-Control-Allow-Methods",
//     "GET, POST, OPTIONS, PUT, PATCH, DELETE"
//   );

//   // Request headers you wish to allow
//   res.setHeader(
//     "Access-Control-Allow-Headers",
//     "X-Requested-With,content-type"
//   );

//   // Set to true if you need the website to include cookies in the requests sent
//   // to the API (e.g. in case you use sessions)
//   res.setHeader("Access-Control-Allow-Credentials", true);

//   // Pass to next layer of middleware
//   next();
// }

// app.use(corsMiddleware);

const corsOptions = {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
  optionsSuccessStatus: 204,
};

app.all("/(*/*)?", cors(corsOptions), function (req, res) {
  const { headers, body, params, path, query, url } = req;
  res.send({
    reqHeaders: headers,
    body,
    params,
    query,
    url,
    path,
    resHeaders: res.getHeaders(),
  });
});

const port = 3000;
app.listen(port, () => {
  console.log(`server listening in ${port} `);
});
